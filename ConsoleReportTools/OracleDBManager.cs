﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleReportTools
{
    public class OracleDBManager
    {
        private OracleConnection _con;
        //private const string connectionString = "User Id={0};Password={1};Data Source=SERVER_247;";
        private const string connectionString = "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.60.18)(PORT=1521))(CONNECT_DATA=(SERVER=dedicated)(SERVICE_NAME=vst)));User ID=esys;Password=esys2";
        private const string OracleDBUser = "esys";
        private const string OracleDBPassword = "esys2";

        public OracleDBManager()
        {
            InitializeDBConnection();
        }

        ~OracleDBManager()
        {
            if (_con != null)
            {
                _con.Close();
                _con.Dispose();
                _con = null;
            }
        }

        private void InitializeDBConnection()
        {
            _con = new OracleConnection();
            _con.ConnectionString = string.Format(connectionString, OracleDBUser, OracleDBPassword);
            _con.Open();
        }
        private void CloseDBConnection()
        {
            if (_con != null)
            {
                _con.Close();
                _con.Dispose();
                _con = null;
            }
        }

        public DataTable excuteSQL(string sql)
        {
            DataTable dt = new DataTable();


            OracleCommand cmd = new OracleCommand();
            cmd.Connection = _con;
            cmd.CommandText = sql;

            OracleDataReader dr = cmd.ExecuteReader();
            //dr.Read();

            dt.Load(dr);

            return dt;
        }
    }
}
