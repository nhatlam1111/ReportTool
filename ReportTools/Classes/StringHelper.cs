﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace ReportTools.Classes
{
    public class StringHelper
    {
        public static object GetValuePropertyObject(object value, string path)
        {
            Type type = value.GetType();
            string str = path;
            char[] chArray = new char[1] { '.' };
            foreach (string name in str.Split(chArray))
            {
                PropertyInfo property = type.GetProperty(name);
                value = property.GetValue(value, (object[])null);
                type = property.PropertyType;
            }
            return value;
        }

        public static string GetValuePropertyString(object value, string path)
        {
            Type type = value.GetType();
            string str1 = "";
            string str2 = path;
            char[] chArray = new char[1] { '.' };
            foreach (string name in str2.Split(chArray))
            {
                if (name.Contains("[") || name.Contains("]"))
                {
                    int num1 = name.IndexOf("[");
                    int num2 = name.IndexOf("]");
                    string path1 = name.Substring(0, num1);
                    int index = int.Parse(Regex.Replace(name.Substring(num1, num2 - num1), "[^0-9]", ""));
                    List<object> objectList = new List<object>();
                    try
                    {
                        objectList = ((IEnumerable)StringHelper.GetValuePropertyObject(value, path1)).Cast<object>().ToList<object>();
                    }
                    catch (Exception ex)
                    {
                    }
                    value = objectList[index];
                    type = value.GetType();
                }
                else
                {
                    PropertyInfo property = type.GetProperty(name);
                    value = property.GetValue(value, (object[])null);
                    type = property.PropertyType;
                }
            }
            if (value.GetType() == typeof(DateTime?) || value.GetType() == typeof(DateTime))
            {
                if (value != null)
                {
                    DateTime dateTime = (DateTime)value;
                    str1 = dateTime.Hour != 0 || dateTime.Minute != 0 ? ((DateTime)value).ToString("dd/MM/yyyy HH:mm") : ((DateTime)value).ToString("dd/MM/yyyy");
                }
            }
            else
                str1 = !(value.GetType() == typeof(int?)) && !(value.GetType() == typeof(Decimal?)) && (!(value.GetType() == typeof(double?)) && !(value.GetType() == typeof(long?))) && (!(value.GetType() == typeof(int)) && !(value.GetType() == typeof(Decimal)) && !(value.GetType() == typeof(double))) && !(value.GetType() == typeof(long)) ? value.ToString() : string.Format("{0:#,##0.####}", value);
            return str1;
        }

        public static string ReadNumber(string value)
        {
            value = value.Replace(".", "").Replace(",", "");
            string str1 = "";
            long result = 0;
            if (!long.TryParse(value, out result))
                return "";
            if (long.Parse(value) == 0L)
                return "không ";
            int num1 = value.Length - 1;
            while (num1 >= 2)
            {
                int num2 = value.Length % 3;
                string str2;
                if ((uint)num2 > 0U)
                {
                    str2 = value.Substring(0, num2);
                    value = value.Remove(0, num2);
                }
                else
                {
                    str2 = value.Substring(0, 3);
                    value = value.Remove(0, 3);
                }
                num1 = value.Length - 1;
                str1 = !str2.Equals("-") ? str1 + StringHelper.Convert3Number(str2) : "Âm";
                if (!string.IsNullOrEmpty(value))
                {
                    if (value.Length == 3 | value.Length == 12 | value.Length == 21 | value.Length == 30 | value.Length == 39)
                    {
                        if (str2 != "000" && str1 != "Âm")
                            str1 += "ngàn ";
                    }
                    else if (value.Length == 6 | value.Length == 15 | value.Length == 24 | value.Length == 33 | value.Length == 42)
                    {
                        if (str2 != "000" && str1 != "Âm")
                            str1 += "triệu ";
                    }
                    else if (value.Length == 9 | value.Length == 18 | value.Length == 27 | value.Length == 36 | value.Length == 45 && (!string.IsNullOrEmpty(str1) && str1 != "Âm"))
                        str1 += "tỷ ";
                }
                else
                    break;
            }
            string str3 = str1 + StringHelper.Convert3Number(value);
            string[] strArray = str3.Trim().Split(' ');
            string str4 = "";
            try
            {
                for (int index = 0; index < strArray[0].Length; ++index)
                    str4 = index != 0 ? str4 + strArray[0][index].ToString() : str4 + strArray[0][index].ToString().ToUpper();
                for (int index = 1; index < strArray.Length; ++index)
                    str4 = str4 + " " + strArray[index];
                return str4;
            }
            catch
            {
                return str3.Trim();
            }
        }

        private static string Convert3Number(string str)
        {
            string str1 = str;
            string str2 = "";
            for (; str.Length > 0; str = str.Remove(0, 1))
            {
                string str3 = str.Substring(0, 1);
                switch (str.Length)
                {
                    case 1:
                        string str4 = str3;
                        if (!(str4 == "5"))
                        {
                            if (str4 == "1")
                            {
                                if (string.IsNullOrEmpty(str2))
                                {
                                    str2 = !(str1.Length == 3 | str1.Length == 2) ? "một " : "lẻ một ";
                                    break;
                                }
                                switch (str1.Length)
                                {
                                    case 2:
                                        str2 = !(str1.Substring(0, 1) == "0") ? (!(str1.Substring(0, 1) == "1") ? str2 + "mốt " : str2 + "một ") : str2 + "lẻ một ";
                                        break;

                                    case 3:
                                        str2 = !(str1.Substring(1, 1) == "0") ? (!(str1.Substring(1, 1) == "1") ? str2 + "mốt " : str2 + "một ") : str2 + "lẻ một ";
                                        break;
                                }
                                break;
                            }
                            if (str3 != "0")
                            {
                                str2 = !string.IsNullOrEmpty(str2) ? (!(str1.Substring(1, 1) == "0") ? str2 + StringHelper.ConvertNumber(str3) : str2 + "lẻ " + StringHelper.ConvertNumber(str3)) : (!(str1.Length == 3 | str1.Length == 2) ? StringHelper.ConvertNumber(str3) : "lẻ " + StringHelper.ConvertNumber(str3));
                                break;
                            }
                            break;
                        }
                        str2 = !string.IsNullOrEmpty(str2) ? (!(str1.Substring(1, 1) == "0") ? str2 + "lăm " : str2 + "lẻ năm ") : (!(str1.Length == 3 | str1.Length == 2) ? StringHelper.ConvertNumber(str3) : "lẻ năm ");
                        break;

                    case 2:
                        if (str3 != "0")
                        {
                            str2 = !string.IsNullOrEmpty(str2) ? (!(str3 == "1") ? str2 + StringHelper.ConvertNumber(str3) + "mươi " : str2 + "mười ") : (!(str3 == "1") ? StringHelper.ConvertNumber(str3) + "mươi " : "mười ");
                            break;
                        }
                        break;

                    case 3:
                        if ((uint)int.Parse(str) > 0U)
                        {
                            str2 = !(str3 != "0") ? "không trăm " : StringHelper.ConvertNumber(str3) + "trăm ";
                            break;
                        }
                        break;
                }
            }
            return str2;
        }

        private static string ConvertNumber(string str)
        {
            switch (str)
            {
                case "0":
                    return "không ";

                case "1":
                    return "một ";

                case "2":
                    return "hai ";

                case "3":
                    return "ba ";

                case "4":
                    return "bốn ";

                case "5":
                    return "năm ";

                case "6":
                    return "sáu ";

                case "7":
                    return "bảy ";

                case "8":
                    return "tám ";

                case "9":
                    return "chín ";

                default:
                    return "";
            }
        }

        public static string ChuyenSo(string number)
        {
            string[] strArray1 = new string[6]
            {
                "",
                "mươi",
                "trăm",
                "nghìn",
                "triệu",
                "tỉ"
            };
            string[] strArray2 = new string[10]
            {
                "không",
                "một",
                "hai",
                "ba",
                "bốn",
                "năm",
                "sáu",
                "bảy",
                "tám",
                "chín"
            };
            number = number.Replace(".", "").Replace(",", "");
            int length = number.Length;
            number += "ss";
            string str = "";
            int num1 = 0;
            int num2;
            for (int index1 = 0; index1 < length; index1 += num2)
            {
                num2 = (length - index1 + 2) % 3 + 1;
                int num3 = 0;
                for (int index2 = 0; index2 < num2; ++index2)
                {
                    if (number[index1 + index2] != '0')
                    {
                        num3 = 1;
                        break;
                    }
                }
                if (num3 == 1)
                {
                    num1 = 1;
                    for (int index2 = 0; index2 < num2; ++index2)
                    {
                        int num4 = 1;
                        switch (number[index1 + index2])
                        {
                            case '0':
                                if (num2 - index2 == 3)
                                    str = str + strArray2[0] + " ";
                                if (num2 - index2 == 2)
                                {
                                    if (number[index1 + index2 + 1] != '0')
                                        str += "lẻ ";
                                    num4 = 0;
                                    break;
                                }
                                break;

                            case '1':
                                if (num2 - index2 == 3)
                                    str = str + strArray2[1] + " ";
                                if (num2 - index2 == 2)
                                {
                                    str += "mười ";
                                    num4 = 0;
                                }
                                if (num2 - index2 == 1)
                                {
                                    int index3 = index1 + index2 != 0 ? index1 + index2 - 1 : 0;
                                    str = number[index3] == '1' || number[index3] == '0' ? str + strArray2[1] + " " : str + "mốt ";
                                    break;
                                }
                                break;

                            case '5':
                                str = index1 + index2 != length - 1 ? str + strArray2[5] + " " : str + "lăm ";
                                break;

                            default:
                                str = str + strArray2[(int)number[index1 + index2] - 48] + " ";
                                break;
                        }
                        if (num4 == 1)
                            str = str + strArray1[num2 - index2 - 1] + " ";
                    }
                }
                if (length - index1 - num2 > 0)
                {
                    if ((length - index1 - num2) % 9 == 0)
                    {
                        if (num1 == 1)
                        {
                            for (int index2 = 0; index2 < (length - index1 - num2) / 9; ++index2)
                                str += "tỉ ";
                        }
                        num1 = 0;
                    }
                    else if ((uint)num3 > 0U)
                        str = str + strArray1[(length - index1 - num2 + 1) % 9 / 3 + 2] + " ";
                }
            }
            if (length == 1 && (number[0] == '0' || number[0] == '5'))
                return strArray2[(int)number[0] - 48];
            return str.Substring(0, 1).ToUpper() + str.Substring(1, str.Length - 1);
        }

        public static string Format(double? number, string groupingSymbol, string decimalSymbol, int decimalPlaces)
        {
            if (!number.HasValue)
                return string.Empty;
            double? nullable = number;
            double num = 0.0;
            if (nullable.GetValueOrDefault() == num && nullable.HasValue)
                return "0";
            NumberFormatInfo numberFormat = new CultureInfo("vi-VN", false).NumberFormat;
            numberFormat.NumberDecimalDigits = decimalPlaces;
            numberFormat.NumberGroupSeparator = groupingSymbol;
            numberFormat.NumberDecimalSeparator = decimalSymbol;
            return number.Value.ToString("N", (IFormatProvider)numberFormat);
        }
    }
}