﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using A = DocumentFormat.OpenXml.Drawing;
using DW = DocumentFormat.OpenXml.Drawing.Wordprocessing;
using PIC = DocumentFormat.OpenXml.Drawing.Pictures;

namespace ReportTools.Classes
{
    public class WordHelper
    {
        public WordprocessingDocument wordprocessingDocument;
        //private WordprocessingDocument saveDocument;
        //private WordprocessingDocument tempDocument;

        private string userID;
        private string docPath;  //path of workDoc
        private string tempPath; //path of saved file
        private Dictionary<string, string> templates;

        //private string fileName;
        private string tempFile;

        private int id = 0;
        private int insertTotal = 0;
        private int insertNo = 0;


        //for password proctection
        int[] InitialCodeArray = { 0xE1F0, 0x1D0F, 0xCC9C, 0x84C0, 0x110C, 0x0E10, 0xF1CE, 0x313E, 0x1872, 0xE139, 0xD40F, 0x84F9, 0x280C, 0xA96A, 0x4EC3 };
        int[,] EncryptionMatrix = new int[15, 7]
        {
            
            /* char 1  */ {0xAEFC, 0x4DD9, 0x9BB2, 0x2745, 0x4E8A, 0x9D14, 0x2A09},
            /* char 2  */ {0x7B61, 0xF6C2, 0xFDA5, 0xEB6B, 0xC6F7, 0x9DCF, 0x2BBF},
            /* char 3  */ {0x4563, 0x8AC6, 0x05AD, 0x0B5A, 0x16B4, 0x2D68, 0x5AD0},
            /* char 4  */ {0x0375, 0x06EA, 0x0DD4, 0x1BA8, 0x3750, 0x6EA0, 0xDD40},
            /* char 5  */ {0xD849, 0xA0B3, 0x5147, 0xA28E, 0x553D, 0xAA7A, 0x44D5},
            /* char 6  */ {0x6F45, 0xDE8A, 0xAD35, 0x4A4B, 0x9496, 0x390D, 0x721A},
            /* char 7  */ {0xEB23, 0xC667, 0x9CEF, 0x29FF, 0x53FE, 0xA7FC, 0x5FD9},
            /* char 8  */ {0x47D3, 0x8FA6, 0x0F6D, 0x1EDA, 0x3DB4, 0x7B68, 0xF6D0},
            /* char 9  */ {0xB861, 0x60E3, 0xC1C6, 0x93AD, 0x377B, 0x6EF6, 0xDDEC},
            /* char 10 */ {0x45A0, 0x8B40, 0x06A1, 0x0D42, 0x1A84, 0x3508, 0x6A10},
            /* char 11 */ {0xAA51, 0x4483, 0x8906, 0x022D, 0x045A, 0x08B4, 0x1168},
            /* char 12 */ {0x76B4, 0xED68, 0xCAF1, 0x85C3, 0x1BA7, 0x374E, 0x6E9C},
            /* char 13 */ {0x3730, 0x6E60, 0xDCC0, 0xA9A1, 0x4363, 0x86C6, 0x1DAD},
            /* char 14 */ {0x3331, 0x6662, 0xCCC4, 0x89A9, 0x0373, 0x06E6, 0x0DCC},
            /* char 15 */ {0x1021, 0x2042, 0x4084, 0x8108, 0x1231, 0x2462, 0x48C4}
          };

        public WordHelper(string docPath, string tempPath, string userID)
        {
            this.docPath = docPath;
            this.tempPath = tempPath;
            this.userID = userID;

            //file template
            //wordprocessingDocument = WordprocessingDocument.Open(docPath, true);

            //file save for user
            if (File.Exists(tempPath))
            {
                File.Delete(tempPath);
            }

            byte[] docAsArray = File.ReadAllBytes(docPath);
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(docAsArray, 0, docAsArray.Length);
                File.WriteAllBytes(tempPath, stream.ToArray());

                stream.Close();
                stream.Dispose();
            }

            //temp file
            //used for replace data from template file and insert to save file
            tempFile = Path.GetTempPath() + "temp_" + DateTime.Now.Ticks + ".docx";
        }

        public WordHelper(Dictionary<string, string> templates, string tempPath, string userID)
        {
            this.tempPath = tempPath;
            this.userID = userID;
            this.templates = templates;
            //file template
            //wordprocessingDocument = WordprocessingDocument.Open(docPath, true);



            //temp file
            //used for replace data from template file and insert to save file
            tempFile = Path.GetTempPath() + "temp_" + DateTime.Now.Ticks + ".docx";
        }

        public void FillData(DataTable data)
        {
            FillData(data, null, null);
        }

        public void FillData(DataTable data, Dictionary<string, DataTable> dynamicTable)
        {
            FillData(data, dynamicTable, null);
        }

        public void FillData(DataTable data, Dictionary<string, string> templateFiles)
        {
            if (data == null || data.Rows.Count <= 0) return;

            if (templateFiles == null || templateFiles.Count <= 0)
            {
                templateFiles = new Dictionary<string, string>();
                templateFiles.Add("0", docPath);
            }

            insertTotal = data.Rows.Count;

            int i = 0;
            while (i < data.Rows.Count) {
                FillTemplate(data.Rows[i], data, i, null, templateFiles);
                i++;
            }
        }

        public void FillData(DataTable data, Dictionary<string, string> templateFiles, int timeout)
        {
            if (data == null || data.Rows.Count <= 0) return;

            if (templateFiles == null || templateFiles.Count <= 0)
            {
                templateFiles = new Dictionary<string, string>();
                templateFiles.Add("0", docPath);
            }

            insertTotal = data.Rows.Count;

            int i = 0;
            while (i < data.Rows.Count)
            {
                FillTemplate(data.Rows[i], data, i, null, templateFiles);
                Thread.Sleep(timeout);
                i++;
            }
        }

        public void FillData(DataTable data, Dictionary<string, DataTable> dynamicTable, Dictionary<string, string> templateFiles)
        {
            if (data == null || data.Rows.Count <= 0) return;

            if (templateFiles == null || templateFiles.Count <= 0)
            {
                templateFiles = new Dictionary<string, string>();
                templateFiles.Add("0", docPath);
            }

            insertTotal = data.Rows.Count;

            int i = 0;
            while (i < data.Rows.Count)
            {
                FillTemplate(data.Rows[i], data, i, dynamicTable, templateFiles);
                i++;
            }

        }

        private void FillTemplate(DataRow data, DataTable dt, int index, Dictionary<string, DataTable> dynamicTable)
        {

            //if (data == null) return;
            //DataRow row = (DataRow)data;

            //if (index == 0)
            //{
            //    WriteDoc(tempPath, row, dt, dynamicTable);
            //    //copy to save file
            //    using (WordprocessingDocument myDoc = WordprocessingDocument.Open(tempPath, true))
            //    {
            //        MainDocumentPart mainPart = myDoc.MainDocumentPart;
            //        //pagebreak;
            //        if (++insertNo < insertTotal)
            //        {
            //            Paragraph PageBreakParagraph = new Paragraph(new Run(new Break() { Type = BreakValues.Page }));
            //            mainPart.Document.Body.Append(PageBreakParagraph);
            //        }
            //        mainPart.Document.Save();
            //    }
            //}
            //else
            //{
            //    //write to tempfile
            //    WriteDoc(tempFile, row, dt, dynamicTable);

            //    //copy to save file
            //    using (WordprocessingDocument myDoc = WordprocessingDocument.Open(tempPath, true))
            //    {
            //        string altChunkId = "AltChunkId1" + (id++);
            //        MainDocumentPart mainPart = myDoc.MainDocumentPart;
            //        AlternativeFormatImportPart chunk = mainPart.AddAlternativeFormatImportPart(AlternativeFormatImportPartType.WordprocessingML, altChunkId);
            //        using (FileStream fileStream = File.Open(tempFile, FileMode.Open))
            //        {
            //            chunk.FeedData(fileStream);
            //        }
            //        AltChunk altChunk = new AltChunk();
            //        altChunk.Id = altChunkId;
            //        mainPart.Document.Body.InsertAfter(altChunk, mainPart.Document.Body.Elements<Paragraph>().LastOrDefault());

            //        //pagebreak;
            //        if (++insertNo < insertTotal)
            //        {
            //            Paragraph PageBreakParagraph = new Paragraph(new Run(new Break() { Type = BreakValues.Page }));
            //            mainPart.Document.Body.Append(PageBreakParagraph);
            //        }
            //        mainPart.Document.Save();
            //    }

            //    DeleteTempFile();
            //}
        }

        private void FillTemplate(DataRow data, DataTable dt, int index, Dictionary<string, DataTable> dynamicTable, Dictionary<string, string> templateFiles)
        {
            if (data == null) return;
            DataRow row = (DataRow)data;

            //get template file from data
            string templateFile = "";
            try
            {
                string template = row["template"].ToString();
                templateFile = templateFiles[template];
            }
            catch
            {
                templateFile = templateFiles["0"];
            }


            if (index == 0)
            {

                // create fist template
                if (File.Exists(tempPath))
                {
                    File.Delete(tempPath);
                }

                byte[] docAsArray = File.ReadAllBytes(templateFile);
                using (MemoryStream stream = new MemoryStream())
                {
                    stream.Write(docAsArray, 0, docAsArray.Length);
                    File.WriteAllBytes(tempPath, stream.ToArray());

                    stream.Close();
                    stream.Dispose();
                }

                WriteDoc(tempPath, row, dt, dynamicTable, templateFile);
                while (IsFileLocked(tempPath))
                {
                    Thread.Sleep(500);
                }


                //copy to save file
                using (WordprocessingDocument myDoc = WordprocessingDocument.Open(tempPath, true))
                {
                    MainDocumentPart mainPart = myDoc.MainDocumentPart;
                    //pagebreak;
                    if (++insertNo < insertTotal)
                    {
                        Paragraph PageBreakParagraph = new Paragraph(new Run(new Break() { Type = BreakValues.Page }));
                        mainPart.Document.Body.Append(PageBreakParagraph);
                    }
                    mainPart.Document.Save();

                    myDoc.Close();
                    myDoc.Dispose();
                }
            }
            else
            {
                //write to tempfile
                WriteDoc(tempFile, row, dt, dynamicTable, templateFile);

                while (IsFileLocked(tempPath))
                {
                    Thread.Sleep(500);
                }
                //copy to save file
                using (WordprocessingDocument myDoc = WordprocessingDocument.Open(tempPath, true))
                {
                    string altChunkId = "AltChunkId1" + (id++);
                    MainDocumentPart mainPart = myDoc.MainDocumentPart;
                    AlternativeFormatImportPart chunk = mainPart.AddAlternativeFormatImportPart(AlternativeFormatImportPartType.WordprocessingML, altChunkId);
                    using (FileStream fileStream = File.Open(tempFile, FileMode.Open))
                    {
                        chunk.FeedData(fileStream);

                        fileStream.Close();
                        fileStream.Dispose();
                    }
                    AltChunk altChunk = new AltChunk();
                    altChunk.Id = altChunkId;
                    mainPart.Document.Body.InsertAfter(altChunk, mainPart.Document.Body.Elements<Paragraph>().LastOrDefault());

                    //try to setup first page margin
                    try
                    {
                        var sectionProps = mainPart.Document.Descendants<SectionProperties>();

                        var mainProp = sectionProps.FirstOrDefault();
                        var mainMargin = mainProp.Descendants<PageMargin>().FirstOrDefault();

                        foreach (var sec in sectionProps)
                        {
                            var pagemargins = sec.Descendants<PageMargin>();

                            foreach (var m in pagemargins)
                            {
                                m.Top = mainMargin.Top;
                                m.Bottom = mainMargin.Bottom;
                                m.Left = mainMargin.Left;
                                m.Right = mainMargin.Right;
                            }
                        }
                    }
                    catch { }


                    //pagebreak;
                    if (++insertNo < insertTotal)
                    {
                        Paragraph PageBreakParagraph = new Paragraph(new Run(new Break() { Type = BreakValues.Page }));
                        mainPart.Document.Body.Append(PageBreakParagraph);
                    }
                    mainPart.Document.Save();
                    myDoc.Close();
                    myDoc.Dispose();
                }

                DeleteTempFile();
            }
        }

        private void WriteDoc(string filePath, DataRow row, DataTable dt, Dictionary<string, DataTable> dynamicTable, string template)
        {
            WriteToTempFile(template, filePath);

            while (IsFileLocked(filePath)) 
            {
                Thread.Sleep(500);
            }

            using (WordprocessingDocument doc = WordprocessingDocument.Open(filePath, true))
            {
                var document = doc.MainDocumentPart.Document;
                var mainPart = doc.MainDocumentPart;

                var paragraphs = document.Descendants<Paragraph>();

                var headers = mainPart.HeaderParts;
                var footers = mainPart.FooterParts;

                foreach (var paragraph in paragraphs)
                {
                    ReplaceParagraph(paragraph, mainPart, row, dt);
                }


                foreach (var header in headers)
                {
                    foreach (var paragraph in header.RootElement.Descendants<Paragraph>())
                    {
                        ReplaceParagraph(paragraph, mainPart, row, dt);
                    }
                }

                foreach (var footer in footers)
                {
                    foreach (var paragraph in footer.RootElement.Descendants<Paragraph>())
                    {
                        ReplaceParagraph(paragraph, mainPart, row, dt);
                    }

                }

                //insert dynamic tables
                if (dynamicTable != null)
                {
                    string searchkey = "";

                    try
                    {
                        searchkey = row["searchkey"].ToString();
                    }
                    catch { }

                    ReplaceTable(dynamicTable, searchkey, document);
                }

                document.Save();

                doc.Close();
                doc.Dispose();
            }
        }

        private void ReplaceParagraph(Paragraph paragraph, MainDocumentPart mainPart, DataRow row, DataTable dt)
        {
            string strText = paragraph.InnerText;
            List<string> columnNames = new List<string>();
            Regex re = new Regex(@"\$[\[](.*?)[\]]");
            var results = re.Matches(strText);

            foreach (Match res in results)
            {
                columnNames.Add(res.Groups[1].Value);
            }

            foreach (string col in columnNames)
            {
                try
                {
                    var type = dt.Columns[col].DataType;

                    //if (col.Contains("image"))
                    if (type == typeof(byte[]))
                    {
                        Size imgSize = new Size(0, 0);
                        byte[] img = null;

                        try { img = (byte[])row[col]; } catch { }

                        try
                        {
                            string[] imgSizeFromData = row[col + "size"].ToString().Split('x');
                            imgSize.Width = int.Parse(imgSizeFromData[0]);
                            imgSize.Height = int.Parse(imgSizeFromData[1]);
                        }
                        catch { }

                        if (imgSize.Width != 0 && imgSize.Height != 0)
                        {
                            ReplaceImage(string.Format("$[{0}]", col), img, imgSize, mainPart, paragraph);
                        }
                        else
                        {
                            ReplaceImage(string.Format("$[{0}]", col), img, mainPart, paragraph);
                        }
                    }
                    else
                    {
                        ReplaceText(string.Format("$[{0}]", col), row[col].ToString(), paragraph);
                    }
                }
                catch
                { }
            }
        }

        private void ReplaceTable(Dictionary<string, DataTable> tableData, string searchkey, Document document)
        {
            var tableProperties = document.Descendants<TableProperties>().Where(tp => tp.TableCaption != null);

            foreach (TableProperties tProp in tableProperties)
            {
                //Console.WriteLine(tProp.TableCaption.Val);
                DataTable data = null;
                IEnumerable<DataRow> dtRow = null;
                List<DataRow> dtRowFinal = null;
                try
                {
                    data = tableData[tProp.TableCaption.Val];
                    dtRow = data.AsEnumerable();

                    if (!string.IsNullOrEmpty(searchkey))
                    {
                        dtRowFinal = dtRow.Where(d => d["searchkey"].ToString() == searchkey).ToList();
                    }
                    else
                    {
                        dtRowFinal = dtRow.ToList();
                    }
                }
                catch
                {
                    Console.WriteLine("Table " + tProp.TableCaption.Val + " not found.");
                }

                Table table = (Table)tProp.Parent;
                var lastRowofTable = table.Elements<TableRow>().LastOrDefault();

                //generate row before insert data
                try
                {
                    for (int i = 0; i < dtRowFinal.Count() - 1; i++)
                    {
                        TableRow rowCopy = (TableRow)lastRowofTable.CloneNode(true);
                        table.AppendChild(rowCopy);
                    }
                }
                catch { }

                //insert data
                var rows = table.Elements<TableRow>().ToList();

                int idxData = 0;
                try { idxData = dtRowFinal.Count - 1; } catch { }

                for (  int idxRow = rows.Count - 1;  idxRow >= 0; )
                {
                    DataRow row = null;
                    try
                    {
                        row = dtRowFinal[idxData];
                    }
                    catch { }
                    var cells = rows[idxRow].Elements<TableCell>();

                    foreach (var cell in cells)
                    {

                        var paragraphs = cell.Descendants<Paragraph>();
                        foreach (var paragraph in paragraphs)
                        {
                            string strText = paragraph.InnerText;
                            List<string> columnNames = new List<string>();
                            Regex re = new Regex(@"\$[\[](.*?)[\]]");
                            var results = re.Matches(strText);

                            foreach (Match res in results)
                            {
                                columnNames.Add(res.Groups[1].Value);
                            }

                            foreach (string col in columnNames)
                            {

                                if (!string.IsNullOrEmpty(col))
                                {
                                    if (col.ToLower() == "no")
                                    {
                                        ReplaceText(string.Format("$[{0}]", col), (idxData + 1) + "", paragraph);
                                    }
                                    else
                                    {
                                        try
                                        { ReplaceText(string.Format("$[{0}]", col), row[col].ToString(), paragraph); }
                                        catch
                                        { ReplaceText(string.Format("$[{0}]", col), "", paragraph); }
                                    }
                                }

                            }
                        }

                    }

                    idxRow--;
                    idxData--;
                }
            }
        }

        private Drawing AddImageToBody(string relationshipId, int width, int height)
        {
            int iWidth = (int)Math.Round((decimal)width * 9525);
            int iHeight = (int)Math.Round((decimal)height * 9525);

            // Define the reference of the image.
            var element =
                 new Drawing(
                     new DW.Inline(
                         new DW.Extent() { Cx = iWidth, Cy = iHeight },
                         new DW.EffectExtent()
                         {
                             LeftEdge = 0L,
                             TopEdge = 0L,
                             RightEdge = 0L,
                             BottomEdge = 0L
                         },
                         new DW.DocProperties()
                         {
                             Id = (UInt32Value)1U,
                             Name = "Picture 1"
                         },
                         new DW.NonVisualGraphicFrameDrawingProperties(
                             new A.GraphicFrameLocks() { NoChangeAspect = true }),
                         new A.Graphic(
                             new A.GraphicData(
                                 new PIC.Picture(
                                     new PIC.NonVisualPictureProperties(
                                         new PIC.NonVisualDrawingProperties()
                                         {
                                             Id = (UInt32Value)0U,
                                             Name = "New Bitmap Image.jpg"
                                         },
                                         new PIC.NonVisualPictureDrawingProperties()),
                                     new PIC.BlipFill(
                                         new A.Blip(
                                             new A.BlipExtensionList(
                                                 new A.BlipExtension()
                                                 {
                                                     Uri = "{28A0092B-C50C-407E-A947-70E740481C1C}"
                                                 })
                                         )
                                         {
                                             Embed = relationshipId,
                                             CompressionState =
                                             A.BlipCompressionValues.Print
                                         },
                                         new A.Stretch(
                                             new A.FillRectangle())),
                                     new PIC.ShapeProperties(
                                         new A.Transform2D(
                                             new A.Offset() { X = 0L, Y = 0L },
                                             new A.Extents() { Cx = iWidth, Cy = iHeight }),
                                         new A.PresetGeometry(new A.AdjustValueList()) { Preset = A.ShapeTypeValues.Rectangle }))
                             )
                             { Uri = "http://schemas.openxmlformats.org/drawingml/2006/picture" })
                     )
                     {
                         DistanceFromTop = (UInt32Value)0U,
                         DistanceFromBottom = (UInt32Value)0U,
                         DistanceFromLeft = (UInt32Value)0U,
                         DistanceFromRight = (UInt32Value)0U,
                         EditId = "50D07946"
                     });

            return element;
        }

        private void ReplaceText(string key, string value, OpenXmlElement node)
        {
            var texts = node.Descendants<Text>();

            string keyName = key.Replace("$[", "").Replace("]", "");
            bool startReplace = false;
            //Text tmpText = null;

            foreach (var item in texts)
            {
                string str = item.Text;

                bool isSpecialCase = string.IsNullOrEmpty(str.Replace("$[", "").Replace("]", "").Trim());

                if (str.Contains("$[") && str != key && startReplace == false && isSpecialCase == true)
                {
                    startReplace = true;
                    item.Text = str.Replace("$[", "");
                    //tmpText = item;
                }
                else if (startReplace == true && str == keyName)
                {
                    //tmpText.Text = str.Replace("$[", "");
                    item.Text = str.Replace(keyName, value);
                }
                else if (str.Contains("]") && str != key && startReplace == true)
                {
                    startReplace = false;
                    item.Text = str.Replace("]", "");
                    continue;
                }
                else if (startReplace == false)
                {
                    item.Text = str.Replace(key, value);
                }
            }
        }

        private void ReplaceImage(string key, byte[] image, MainDocumentPart mainPart, OpenXmlElement node)
        {
            if (image == null || image.Length <= 0)
            {
                ReplaceText(key, "", node);
                return;
            }

            // insert image
            ImagePart imagePart = mainPart.AddImagePart(ImagePartType.Jpeg);
            Bitmap bmp = null;
            using (Stream stream = new MemoryStream(image))
            {
                imagePart.FeedData(stream);

                stream.Close();
                stream.Dispose();
            }

            using (Stream stream = new MemoryStream(image))
            {
                bmp = new Bitmap(stream);

                stream.Close();
                stream.Dispose();
            }

            //image size
            Size imgSize = new Size(bmp != null ? bmp.Width : 200, bmp != null ? bmp.Height : 200);
            var imageInsert = AddImageToBody(mainPart.GetIdOfPart(imagePart), imgSize.Width, imgSize.Height);
            node.AppendChild(new Run(imageInsert.CloneNode(true)));

            ReplaceText(key, "", node);
        }

        private void ReplaceImage(string key, byte[] image, Size imageSize, MainDocumentPart mainPart, OpenXmlElement node)
        {
            if (image == null || image.Length <= 0)
            {
                ReplaceText(key, "", node);
                return;
            }

            // insert image
            ImagePart imagePart = mainPart.AddImagePart(ImagePartType.Jpeg);
            Bitmap bmp = null;
            using (Stream stream = new MemoryStream(image))
            {
                imagePart.FeedData(stream);

                stream.Close();
                stream.Dispose();
            }

            using (Stream stream = new MemoryStream(image))
            {
                bmp = new Bitmap(stream);

                stream.Close();
                stream.Dispose();
            }

            var imageInsert = AddImageToBody(mainPart.GetIdOfPart(imagePart), imageSize.Width, imageSize.Height);
            node.AppendChild(new Run(imageInsert.CloneNode(true)));

            ReplaceText(key, "", node);
        }

        private void WriteToTempFile(string templateFile, string filePath)
        {
            byte[] docAsArray = File.ReadAllBytes(templateFile);

            using (MemoryStream stream = new MemoryStream())
            {
                DeleteTempFile(filePath);

                stream.Write(docAsArray, 0, docAsArray.Length);
                File.WriteAllBytes(filePath, stream.ToArray());

                stream.Close();
                stream.Dispose();
            }
        }

        private void DeleteTempFile()
        {
            if (File.Exists(tempFile))
            {
                File.Delete(tempFile);
            }
        }

        private void DeleteTempFile(string filePath)
        {
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
        }


        private bool IsFileLocked(string filePath)
        {
            try
            {
                using (FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    stream.Close();
                }
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }

            //file is not locked
            return false;
        }



        //public void SetLock(string password, DocumentProtectionValues objLockType)
        public void SetLock(string password)
        {
            try
            {
                using (WordprocessingDocument _objDoc = WordprocessingDocument.Open(tempPath, true))
                {
                  DocumentProtection documentProtection = new DocumentProtection();

                    documentProtection.Edit = DocumentProtectionValues.Forms;

                    // Generate the Salt
                    byte[] arrSalt = new byte[16];
                    RandomNumberGenerator rand = new RNGCryptoServiceProvider();
                    rand.GetNonZeroBytes(arrSalt);

                    //Array to hold Key Values
                    byte[] generatedKey = new byte[4];

                    //Maximum length of the password is 15 chars.
                    int intMaxPasswordLength = 15;

                    string strPassword = password;

                    if (!String.IsNullOrEmpty(strPassword))
                    {
                        // Truncate the password to 15 characters
                        strPassword = strPassword.Substring(0, Math.Min(strPassword.Length, intMaxPasswordLength));

                        // Construct a new NULL-terminated string consisting of single-byte characters:
                        //  -- > Get the single-byte values by iterating through the Unicode characters of the truncated Password.
                        //   --> For each character, if the low byte is not equal to 0, take it. Otherwise, take the high byte.

                        byte[] arrByteChars = new byte[strPassword.Length];

                        for (int intLoop = 0; intLoop < strPassword.Length; intLoop++)
                        {
                            int intTemp = Convert.ToInt32(strPassword[intLoop]);
                            arrByteChars[intLoop] = Convert.ToByte(intTemp & 0x00FF);
                            if (arrByteChars[intLoop] == 0)
                                arrByteChars[intLoop] = Convert.ToByte((intTemp & 0xFF00) >> 8);
                        }

                        // Compute the high-order word of the new key:

                        // --> Initialize from the initial code array (see below), depending on the strPassword’s length. 
                        int intHighOrderWord = InitialCodeArray[arrByteChars.Length - 1];

                        // --> For each character in the strPassword:
                        //      --> For every bit in the character, starting with the least significant and progressing to (but excluding) 
                        //          the most significant, if the bit is set, XOR the key’s high-order word with the corresponding word from 
                        //          the Encryption Matrix

                        for (int intLoop = 0; intLoop < arrByteChars.Length; intLoop++)
                        {
                            int tmp = intMaxPasswordLength - arrByteChars.Length + intLoop;
                            for (int intBit = 0; intBit < 7; intBit++)
                            {
                                if ((arrByteChars[intLoop] & (0x0001 << intBit)) != 0)
                                {
                                    intHighOrderWord ^= EncryptionMatrix[tmp, intBit];
                                }
                            }
                        }

                        // Compute the low-order word of the new key:

                        // Initialize with 0
                        int intLowOrderWord = 0;

                        // For each character in the strPassword, going backwards
                        for (int intLoopChar = arrByteChars.Length - 1; intLoopChar >= 0; intLoopChar--)
                        {
                            // low-order word = (((low-order word SHR 14) AND 0x0001) OR (low-order word SHL 1) AND 0x7FFF)) XOR character
                            intLowOrderWord = (((intLowOrderWord >> 14) & 0x0001) | ((intLowOrderWord << 1) & 0x7FFF)) ^ arrByteChars[intLoopChar];
                        }

                        // Lastly,low-order word = (((low-order word SHR 14) AND 0x0001) OR (low-order word SHL 1) AND 0x7FFF)) XOR strPassword length XOR 0xCE4B.
                        intLowOrderWord = (((intLowOrderWord >> 14) & 0x0001) | ((intLowOrderWord << 1) & 0x7FFF)) ^ arrByteChars.Length ^ 0xCE4B;

                        // Combine the Low and High Order Word
                        int intCombinedkey = (intHighOrderWord << 16) + intLowOrderWord;

                        // The byte order of the result shall be reversed [Example: 0x64CEED7E becomes 7EEDCE64. end example],
                        // and that value shall be hashed as defined by the attribute values.

                        for (int intTemp = 0; intTemp < 4; intTemp++)
                        {
                            generatedKey[intTemp] = Convert.ToByte(((uint)(intCombinedkey & (0x000000FF << (intTemp * 8)))) >> (intTemp * 8));
                        }
                    }

                    // Implementation Notes List:
                    // --> In this third stage, the reversed byte order legacy hash from the second stage shall be converted to Unicode hex 
                    // --> string representation 
                    StringBuilder sb = new StringBuilder();
                    for (int intTemp = 0; intTemp < 4; intTemp++)
                    {
                        sb.Append(Convert.ToString(generatedKey[intTemp], 16));
                    }
                    generatedKey = Encoding.Unicode.GetBytes(sb.ToString().ToUpper());

                    // Implementation Notes List:
                    //Word appends the binary form of the salt attribute and not the base64 string representation when hashing
                    // Before calculating the initial hash, you are supposed to prepend (not append) the salt to the key
                    byte[] tmpArray1 = generatedKey;
                    byte[] tmpArray2 = arrSalt;
                    byte[] tempKey = new byte[tmpArray1.Length + tmpArray2.Length];
                    Buffer.BlockCopy(tmpArray2, 0, tempKey, 0, tmpArray2.Length);
                    Buffer.BlockCopy(tmpArray1, 0, tempKey, tmpArray2.Length, tmpArray1.Length);
                    generatedKey = tempKey;


                    // Iterations specifies the number of times the hashing function shall be iteratively run (using each
                    // iteration's result as the input for the next iteration).
                    int iterations = 50000;

                    // Implementation Notes List:
                    //Word requires that the initial hash of the password with the salt not be considered in the count.
                    //    The initial hash of salt + key is not included in the iteration count.
                    HashAlgorithm sha1 = new SHA1Managed();
                    generatedKey = sha1.ComputeHash(generatedKey);
                    byte[] iterator = new byte[4];
                    for (int intTmp = 0; intTmp < iterations; intTmp++)
                    {

                        //When iterating on the hash, you are supposed to append the current iteration number.
                        iterator[0] = Convert.ToByte((intTmp & 0x000000FF) >> 0);
                        iterator[1] = Convert.ToByte((intTmp & 0x0000FF00) >> 8);
                        iterator[2] = Convert.ToByte((intTmp & 0x00FF0000) >> 16);
                        iterator[3] = Convert.ToByte((intTmp & 0xFF000000) >> 24);

                        generatedKey = concatByteArrays(iterator, generatedKey);
                        generatedKey = sha1.ComputeHash(generatedKey);
                    }

                    DocumentFormat.OpenXml.OnOffValue docProtection = new DocumentFormat.OpenXml.OnOffValue(true);
                    documentProtection.Enforcement = docProtection;

                    documentProtection.CryptographicAlgorithmClass = CryptAlgorithmClassValues.Hash;
                    documentProtection.CryptographicProviderType = CryptProviderValues.RsaFull;
                    documentProtection.CryptographicAlgorithmType = CryptAlgorithmValues.TypeAny;
                    documentProtection.CryptographicAlgorithmSid = 4; // SHA1
                    //    The iteration count is unsigned
                    UInt32 uintVal = new UInt32();
                    uintVal = (uint)iterations;
                    documentProtection.CryptographicSpinCount = uintVal;
                    documentProtection.Hash = Convert.ToBase64String(generatedKey);
                    documentProtection.Salt = Convert.ToBase64String(arrSalt);
                    _objDoc.MainDocumentPart.DocumentSettingsPart.Settings.AppendChild(documentProtection);
                    _objDoc.MainDocumentPart.DocumentSettingsPart.Settings.Save();
                    _objDoc.Close();
                    _objDoc.Dispose();
                }
            }
            catch (Exception ex)
            {

                
            }
        }


        //public void CreatePasswordRestriction(string password)
        //{
        //    using (WordprocessingDocument doc = WordprocessingDocument.Open(tempPath, true))
        //    {
        //        var document = doc.MainDocumentPart.Document;
        //        ApplyDocumentProtection(doc, password);

        //        SdtBlock sdtBlock =
        //                                document
        //                                .MainDocumentPart
        //                                .Document
        //                                .Body
        //                                .Descendants<SdtBlock>()
        //                                //.Where(b => b.SdtProperties.GetFirstChild<Tag>().Val == "myTagName")
        //                               .SingleOrDefault()
        //                                ;

        //        var contentLock = new Lock { Val = LockingValues.SdtContentLocked };
        //        sdtBlock.SdtProperties.AppendChild(contentLock);

        //        document.Save();
        //    }
        //}


        private byte[] concatByteArrays(byte[] array1, byte[] array2)
        {
            byte[] result = new byte[array1.Length + array2.Length];
            Buffer.BlockCopy(array2, 0, result, 0, array2.Length);
            Buffer.BlockCopy(array1, 0, result, array2.Length, array1.Length);
            return result;
        }


        ////reference: https://docs.microsoft.com/vi-vn/archive/blogs/vsod/how-to-set-the-editing-restrictions-in-word-using-open-xml-sdk-2-0
        //// Main implementation
        //private void ApplyDocumentProtection(WordprocessingDocument wdDocument, string strPassword)
        //{
        //    // Generate the Salt
        //    byte[] arrSalt = new byte[16];
        //    RandomNumberGenerator rand = new RNGCryptoServiceProvider();
        //    rand.GetNonZeroBytes(arrSalt);

        //    //Array to hold Key Values
        //    byte[] generatedKey = new byte[4];

        //    //Maximum length of the password is 15 chars.
        //    int intMaxPasswordLength = 15;


        //    if (!String.IsNullOrEmpty(strPassword))
        //    {
        //        // Truncate the password to 15 characters
        //        strPassword = strPassword.Substring(0, Math.Min(strPassword.Length, intMaxPasswordLength));

        //        // Construct a new NULL-terminated string consisting of single-byte characters:
        //        //  -- > Get the single-byte values by iterating through the Unicode characters of the truncated Password.
        //        //   --> For each character, if the low byte is not equal to 0, take it. Otherwise, take the high byte.

        //        byte[] arrByteChars = new byte[strPassword.Length];

        //        for (int intLoop = 0; intLoop < strPassword.Length; intLoop++)
        //        {
        //            int intTemp = Convert.ToInt32(strPassword[intLoop]);
        //            arrByteChars[intLoop] = Convert.ToByte(intTemp & 0x00FF);
        //            if (arrByteChars[intLoop] == 0)
        //                arrByteChars[intLoop] = Convert.ToByte((intTemp & 0xFF00) >> 8);
        //        }

        //        // Compute the high-order word of the new key:

        //        // --> Initialize from the initial code array (see below), depending on the strPassword’s length. 
        //        int intHighOrderWord = InitialCodeArray[arrByteChars.Length - 1];

        //        // --> For each character in the strPassword:
        //        //      --> For every bit in the character, starting with the least significant and progressing to (but excluding) 
        //        //          the most significant, if the bit is set, XOR the key’s high-order word with the corresponding word from 
        //        //          the Encryption Matrix

        //        for (int intLoop = 0; intLoop < arrByteChars.Length; intLoop++)
        //        {
        //            int tmp = intMaxPasswordLength - arrByteChars.Length + intLoop;
        //            for (int intBit = 0; intBit < 7; intBit++)
        //            {
        //                if ((arrByteChars[intLoop] & (0x0001 << intBit)) != 0)
        //                {
        //                    intHighOrderWord ^= EncryptionMatrix[tmp, intBit];
        //                }
        //            }
        //        }

        //        // Compute the low-order word of the new key:

        //        // Initialize with 0
        //        int intLowOrderWord = 0;

        //        // For each character in the strPassword, going backwards
        //        for (int intLoopChar = arrByteChars.Length - 1; intLoopChar >= 0; intLoopChar--)
        //        {
        //            // low-order word = (((low-order word SHR 14) AND 0x0001) OR (low-order word SHL 1) AND 0x7FFF)) XOR character
        //            intLowOrderWord = (((intLowOrderWord >> 14) & 0x0001) | ((intLowOrderWord << 1) & 0x7FFF)) ^ arrByteChars[intLoopChar];
        //        }

        //        // Lastly,low-order word = (((low-order word SHR 14) AND 0x0001) OR (low-order word SHL 1) AND 0x7FFF)) XOR strPassword length XOR 0xCE4B.
        //        intLowOrderWord = (((intLowOrderWord >> 14) & 0x0001) | ((intLowOrderWord << 1) & 0x7FFF)) ^ arrByteChars.Length ^ 0xCE4B;

        //        // Combine the Low and High Order Word
        //        int intCombinedkey = (intHighOrderWord << 16) + intLowOrderWord;

        //        // The byte order of the result shall be reversed [Example: 0x64CEED7E becomes 7EEDCE64. end example],
        //        // and that value shall be hashed as defined by the attribute values.

        //        for (int intTemp = 0; intTemp < 4; intTemp++)
        //        {
        //            generatedKey[intTemp] = Convert.ToByte(((uint)(intCombinedkey & (0x000000FF << (intTemp * 8)))) >> (intTemp * 8));
        //        }
        //    }

        //    // Implementation Notes List:
        //    // --> In this third stage, the reversed byte order legacy hash from the second stage shall be converted to Unicode hex 
        //    // --> string representation 
        //    StringBuilder sb = new StringBuilder();
        //    for (int intTemp = 0; intTemp < 4; intTemp++)
        //    {
        //        sb.Append(Convert.ToString(generatedKey[intTemp], 16));
        //    }
        //    generatedKey = Encoding.Unicode.GetBytes(sb.ToString().ToUpper());

        //    // Implementation Notes List:
        //    //Word appends the binary form of the salt attribute and not the base64 string representation when hashing
        //    // Before calculating the initial hash, you are supposed to prepend (not append) the salt to the key
        //    byte[] tmpArray1 = generatedKey;
        //    byte[] tmpArray2 = arrSalt;
        //    byte[] tempKey = new byte[tmpArray1.Length + tmpArray2.Length];
        //    Buffer.BlockCopy(tmpArray2, 0, tempKey, 0, tmpArray2.Length);
        //    Buffer.BlockCopy(tmpArray1, 0, tempKey, tmpArray2.Length, tmpArray1.Length);
        //    generatedKey = tempKey;


        //    // Iterations specifies the number of times the hashing function shall be iteratively run (using each
        //    // iteration's result as the input for the next iteration).
        //    int iterations = 50000;

        //    // Implementation Notes List:
        //    //Word requires that the initial hash of the password with the salt not be considered in the count.
        //    //    The initial hash of salt + key is not included in the iteration count.
        //    HashAlgorithm sha1 = new SHA1Managed();
        //    generatedKey = sha1.ComputeHash(generatedKey);
        //    byte[] iterator = new byte[4];
        //    for (int intTmp = 0; intTmp < iterations; intTmp++)
        //    {

        //        //When iterating on the hash, you are supposed to append the current iteration number.
        //        iterator[0] = Convert.ToByte((intTmp & 0x000000FF) >> 0);
        //        iterator[1] = Convert.ToByte((intTmp & 0x0000FF00) >> 8);
        //        iterator[2] = Convert.ToByte((intTmp & 0x00FF0000) >> 16);
        //        iterator[3] = Convert.ToByte((intTmp & 0xFF000000) >> 24);

        //        generatedKey = concatByteArrays(iterator, generatedKey);
        //        generatedKey = sha1.ComputeHash(generatedKey);
        //    }

        //    // Apply the element
        //    DocumentProtection documentProtection = new DocumentProtection();
        //    documentProtection.Edit = DocumentProtectionValues.ReadOnly;

        //    OnOffValue docProtection = new OnOffValue(true);
        //    documentProtection.Enforcement = docProtection;

        //    documentProtection.CryptographicAlgorithmClass = CryptAlgorithmClassValues.Hash;
        //    documentProtection.CryptographicProviderType = CryptProviderValues.RsaFull;
        //    documentProtection.CryptographicAlgorithmType = CryptAlgorithmValues.TypeAny;
        //    documentProtection.CryptographicAlgorithmSid = 4; // SHA1
        //    //    The iteration count is unsigned
        //    UInt32Value uintVal = new UInt32Value();
        //    uintVal.Value = (uint)iterations;
        //    documentProtection.CryptographicSpinCount = uintVal;
        //    documentProtection.Hash = Convert.ToBase64String(generatedKey);
        //    documentProtection.Salt = Convert.ToBase64String(arrSalt);
        //    wdDocument.MainDocumentPart.DocumentSettingsPart.Settings.AppendChild(documentProtection);
        //    wdDocument.MainDocumentPart.DocumentSettingsPart.Settings.Save();

        //}

    }
}