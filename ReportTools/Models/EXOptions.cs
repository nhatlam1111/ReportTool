﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportTools.Models
{
    public class EXOptions
    {
        //sheet option
        public List<EXSheet> sheets;

        //more options

        public EXOptions()
        {
            sheets = new List<EXSheet>();
        }
    }
}
