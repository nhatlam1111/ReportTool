﻿using NPOI.SS.UserModel;
using NPOI.SS.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportTools.Models
{
    public class EXSheet
    {
        public int SheetIndex;
        public string SheetName;
        public List<EXMerge> merges;
        public EXMerge GrandTotal;

        public EXSheet()
        {
            SheetIndex = 0;
            SheetName = "Sheet0";
            merges = new List<EXMerge>();
            GrandTotal = new EXMerge();
        }
    }

    public class EXMerge
    {
        public string ColumnName;
        public int ColumnIndex;
        public bool isTotal;
        public bool isTotalTop;
        public bool isHaveRowTotal;
        public SubTotalType SubTotalType;
        public IFont SubTotalFont;
        public Color CellColor;
        public int MergeColumnFrom;
        public int MergeColumnTo;
        public string TotalText;
        public TotalTextType TotalTextType;

        public List<EXColumn> ColumnMergeOptions;

        public EXMerge()
        {
            isTotal = false;
            isTotalTop = false;
            isHaveRowTotal = false;
            ColumnName = "";
            SubTotalFont = null;
            CellColor = Color.Transparent;
            TotalTextType = TotalTextType.Default;

            ColumnMergeOptions = new List<EXColumn>();
        }
    }

    public class EXColumn
    {
        public string ColumnName;
        public int ColumnIndex;
        public SubTotalType SubTotalType;
        public ICellStyle CellStyle;
    }

    public enum SubTotalType
    {
        NoValue,
        Count,
        SumValue,
        Average
    }

    public enum TotalTextType
    {
        NoValue,
        Default,
        GroupValue,
        GroupValueAndCount,
        Input
    }
}
